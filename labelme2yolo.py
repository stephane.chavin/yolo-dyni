"""Converts LabelMe annotations to YOLO format"""

import os
import json
import argparse
from glob import glob
import utils


def main(arguments):
    """
    Launch the processing on each .txt.
    :param arguments (args): Argument
    """
    yolo_names = set()
    for folder in ['images', 'labels', 'images/all']: # Create saving directory
        utils.create_directory(os.path.join(arguments.directory, folder))

    # For each annotation, convert into YOLO format
    for labelme_annotation_path in glob(f'{arguments.path_to_data}/*.json'):
        utils.labelme2yolo(labelme_annotation_path, arguments.directory)
        # Add all the information in the .txt file
        with open(labelme_annotation_path, 'r', encoding='utf-8') as labelme_annotation_file:
            labelme_annotation = json.load(labelme_annotation_file)

            for shape in labelme_annotation['shapes']:
                yolo_names.add(shape['label'])

    # Write YOLO names and save it
    yolo_names_path = os.path.join(arguments.directory, 'custom.names')
    with open(yolo_names_path, 'w', encoding='utf-8') as yolo_names_file:
        yolo_names_file.write('\n'.join(yolo_names))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Convert LabelMe annotations to YOLO compatible')
    parser.add_argument('path_to_data', type=utils.arg_directory,
                        help='Path to LabelMe annotations')
    parser.add_argument('directory', type=utils.arg_directory,
                        help='Directory to which YOLO annotations will be stored')
    args = parser.parse_args()

    main(args)
