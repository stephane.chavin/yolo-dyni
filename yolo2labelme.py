"""Converts YOLO detections to LabelMe format"""

import argparse
import os
import utils


def main(arguments):
    """
    Launch the processing on each .json.
    :param arguments (args): Argument
    """
    if arguments.directory is None:
        arguments.directory = arguments.path_to_txt
    utils.process_json_files(
        arguments.path_to_txt, arguments.path_to_img,
        arguments.directory)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter, description='Convert YOLO annotation to .json file')
    parser.add_argument('path_to_txt', type=utils.arg_directory,
                        help='Path to the folder containing the .txt files')
    parser.add_argument('path_to_img', type=utils.arg_directory,
                        help='Path to the folder containing the .jpg images')
    parser.add_argument('-d', '--directory', type=utils.arg_directory, help='Directory to which modified .json files will be stored'
                        'if no argument, directory will be same as path_to_txt', required=False,
                        default=None)
    args = parser.parse_args()

    # Convert the .json file into .txt in the labelme format
    os.system(
        f'globox convert {args.path_to_txt} {args.directory} --format yolo --save_fmt labelme --img_folder {args.path_to_img}')
    main(args)
